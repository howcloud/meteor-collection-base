// TODO: Eventually should change the model system to be more composable - mixins should be actual mixins rather than bastardised extensions to classes etc
// In essence - this package is a nice start and does the trick (to a certain extent) but there is a lot of work to go

Package.describe({
	name: 'howcloud:collection-base',
	summary: "Builds on the Meteor collection system to offer wrapped model-like functionality and client vs server representations of data"
});

Package.on_use(function (api) {
	
	/* Package Dependencies */

	api.use('underscore', ['client', 'server']);
	api.use('ddp', ['client', 'server']);
	api.use('mongo', ['client', 'server']);

	api.use('meteorhacks:unblock', ['server']); // for publish unblock calls

	/* Add files */

	api.add_files('lib/lib/inheritance.js', ['client', 'server']);

	api.add_files('lib/Collection/Collection_shared.js', ['client', 'server']);
	api.add_files('lib/Collection/Collection_server.js', 'server');
	api.add_files('lib/Collection/Collection_client.js', 'client');

	api.add_files('lib/Document/Document_shared.js', ['client', 'server']);
	api.add_files('lib/Document/Document_server.js', 'server');
	api.add_files('lib/Document/Document_client.js', 'client');

	api.add_files('lib/Mixins/Codename_Mixin.js', ['client', 'server']);
	api.add_files('lib/Mixins/Owned_Mixin.js', ['client', 'server']);
	api.add_files('lib/Mixins/User_Mixin.js', ['client', 'server']);

	/* Export */

	api.export('Collections', ['client', 'server']);
	api.export('AllCollections', ['client', 'server']); // technically only needed on client (or at least that was the only place we used it before)

});