/** Mixin to Collection **/

var _Collection_Owned = {

	/** Get by owner **/

	getByOwner: function (_owner_id, projection) {
		return this.getByOwner_cursor(_owner_id, projection).fetch();
	},
	getByOwner_cursor: function (_owner_id, projection) {
		return this.collection.find({owner_id: _owner_id}, projection);
	},

}

/** Export mixin **/

Collections.mixins.owned = {
	extend_collection: _Collection_Owned,
	extend_document_prototype: null,
}