/** Mixin to Collection **/

var _Collection_User = {

	/** Get by owner **/

	getByUser: function (_user_id, projection) {
		return this.getByUser_cursor(_user_id, projection).fetch();
	},
	getByUser_cursor: function (_user_id, projection) {
		return this.collection.find({user_id: _user_id}, projection);
	},

}

/** Export mixin **/

Collections.mixins.user = {
	extend_collection: _Collection_User,
	extend_document_prototype: null,
}