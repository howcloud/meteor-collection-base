/** Mixin to Collection **/

var _Collection_Codename = {

	/** Get by codename **/

	getByCodename: function (_codename, projection) {
		return this.collection.findOne({codename: _codename}, projection);
	},
	getByCodename_cursor: function (_codename, projection) {
		return this.collection.find({codename: _codename}, projection);
	},

	/** Get by identfier **/
	// Now we have both _ids and codenames we make getting by providing either of those possible (we default to id if we cannot find by codename)

	getByIdentifier: function (_identifier, projection) {
		return this.getByIdentifier_cursor(_identifier, projection).fetch()[0];
	},
	getByIdentifier_cursor: function (_identifier, projection) {
		_identifier = _identifier || '';

		var codename = this.collection.find({ // first try and find by codename
			codename: _identifier.toLowerCase()
		}, projection);
		
		if (codename.fetch().length) return codename;
		
		return this.collection.find(_identifier, projection); // by default return by id
	},

}

/** Mixin to Collection's Document Prototype **/

var _DocumentPrototpye_Mixin_Codename = {};

if (Meteor.isServer) {

	_DocumentPrototpye_Mixin_Codename.pushCodename = function (fromName) {
		var codename = fromName.toLowerCase().replace(/[\t\r\n\s]+/g, "-").replace(/[^a-zA-Z0-9_-]/g, "");

		var num = 0;
		while (true) { // make codename globally unique, for now
			tryCodename = codename + (num == 0 ? '' : '-' + num);

			if (_.indexOf(this.codename, tryCodename) != -1) { // is it already a codename for this instance?
				// we pull and push it to make sure it's the last index of the codename array (this is the index we use for generating URLs)
				// Hacky implementation as we cannot duplicate field names across mongo modifiers - is there an alternate way?

				this._collection.collection.update(this._id,{
					$pull: {codename: tryCodename}
				});
				this._collection.collection.update(this._id,{
					$push: {codename: tryCodename}
				});

				return tryCodename;
			} else {
				if (!this._collection.getByCodename(tryCodename)) break;
				num++;
			}
		}

		this._collection.collection.update(this._id, {
			$push: {'codename': tryCodename.toLowerCase()}
		});

		this.codename = this.codename || []; // in place update as we want to return straight away from the new method
		this.codename.push(tryCodename);

		return tryCodename;
	}

} else if (Meteor.isClient) {

}

/** Export mixin **/

Collections.mixins.codename = {
	init_mixin: function (collection) {
		if (Meteor.isServer) {
			collection.collection._ensureIndex({
				'codename': 1
			});
		}
	},

	extend_collection: _Collection_Codename,
	extend_document_prototype: _DocumentPrototpye_Mixin_Codename
}