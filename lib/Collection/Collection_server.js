/** New **/
// this is often overriden to introduce custom functionality/validation

Collection_Prototype.new = function (data) {
	return this.getById(this.collection.insert(data));
}

/** Publish Representations of Arrays **/

Collection_Prototype.getPublishRepresentations = function (docs, _user_id) {
	var rt = [];
	var args = Array.prototype.slice.call(arguments).slice(1);

	_.each(docs, function (doc) {
		// var vars = [doc];
		var rep = doc.getPublishRepresentation.apply(doc, args);

		if (rep) rt.push(rep);
	});

	return rt;
}

// getConditionalPublishRepresentation:
// condition is a function, returns true if should include passed document
// NOTE: Unecessary, just return null in getPublishRepresentation to not include this in an array

/*

Collection_Prototype.getConditionalPublishRepresentation = function (condition, docs) {
	var rt = [];

	_.each(docs, function (doc) {
		if (condition(doc)) {
			
		}
	});

	return rt;
}

*/

/*******************************************************************************************************************************/

/** Publish System Wrap **/

// We override the meteor publish system to manually handle publications so that we can push out publishable representations (in relevant cases)

Meteor.publish = _.wrap(Meteor.publish, function (_meteorPublish, name, fn) {
	fn = _.wrap(fn, function (_fn) {
		if (this.unblock) this.unblock(); // we unblock all publishes by default using meteorhacks package - is this a bad idea?

		var args = Array.prototype.slice.call(arguments);

		var cursor = _fn.apply(this, args.slice(1));
		if (!cursor) return null;

		var remainingCursor = _handlePublish(cursor, this);
		
		if (!remainingCursor || remainingCursor.length == 0) {
			this.ready();
		} else {
			return remainingCursor; // need meteor to manually handle cursors
		}
	});

	_meteorPublish(name, fn);
});

var _handlePublish = function (cursor, publisher) {
	if (cursor instanceof Array) { // => array of cursors
		var rt = [];

		_.each(cursor, function (_cursor) {
			var remainingCursor = _handlePublish(_cursor, publisher);
			if (remainingCursor) rt.push(remainingCursor);
		});

		return rt;
	} else {
		
		var collectionName = cursor.collection ? cursor.collection.name : cursor._cursorDescription.collectionName;
		var collection = Collections._collections[collectionName];
		if (!collection) return cursor;

		// Only manually handle the publish if we have a different publish representation for this collection
		if (collection.getDocPrototype().getPublishRepresentation === undefined) return cursor;

		//if (collection.getDocPrototype().getPublishRepresentation !== undefined) {
			//if (cursor._cursorDescription !== undefined && cursor._cursorDescription.options !== undefined && cursor._cursorDescription.options.fields !== undefined) return cursor;
		//}

		var observer = cursor.observeChanges({
			added: function (id, fields) {
				//publisher.added(collection.name, id, fields);
				_publishRepresentation_handler(collection, publisher.added, publisher, id, fields);
			},
			removed: function (id) {
				publisher.removed(collection.name, id);
			},
			changed: function (id, fields) {
				//publisher.changed(collection.name, id, fields);
				_publishRepresentation_handler(collection, publisher.changed, publisher, id, fields);
			}
		});

		publisher.onStop(function () {
			observer.stop(); // stop observing changes when client unsubscribes from publication
		});
	}
}

var _publishRepresentation_handler = function (collection, actionfn, publisher, id, fields) {
	var doc = collection.getById(id);
	if (!doc) return; // handles an edge case

	var publishDoc = doc.getPublishRepresentation(publisher.userId);
	if (!publishDoc) return; // returning null => do not publish

	actionfn.call(publisher, collection.name, id, publishDoc);
}