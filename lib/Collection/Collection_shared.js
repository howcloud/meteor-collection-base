AllCollections = {} // name to actual underlying meteor collection lookup

Collections = {

	_collections: [], // reference to all created collections by name
	mixins: [], // array of possible mixins to mixin to new collections using the mixin option key

	init: function (name, options) {
		options = options || {};

		/* Create Instance */

		var collection = Object.create(_.clone(Collection_Prototype)); // we clone the prototype so that we can extend this collection without extending all other collections
		this._collections[name] = collection;
		collection.name = name;

		/* Create Underlying Collection */

		var _transformFunction = function (data) {
			var transformed = _collection_documentTransform(collection, data);
		
			if (options.transform) transformed = options.transform(transformed); // allows us to do additional setup on a document on a per collection basis
		
			return transformed;
		}

		if (options.collection) {
			options.collection._transform = _transformFunction;
			collection.collection = options.collection;
		} else {
			collection.collection = new Meteor.Collection(name, {
				transform: _transformFunction
			});
		}

		/* Options */

		if (options.mixins) {
			_.each(options.mixins, function (mixin) {
				if (mixin.init_mixin) mixin.init_mixin(collection);

				if (mixin.extend_document_prototype) collection.extendDocumentPrototype(mixin.extend_document_prototype);
				if (mixin.extend_collection) collection.extendCollection(mixin.extend_collection);
			});
		}

		/* Save to AllCollections */

		AllCollections[name] = collection.collection;

		/* Return */

		return collection;
	},

	// sortDocuments
	// Used to sort a mixed array of documents from different collections

	sortDocuments: function (list) {
		return _.sortBy(list, function (_document) {
			if (_document.sortValue) return _document.sortValue().toLowerCase();

			return '';
		});
	}

}

var _collection_documentTransform = function (collection, data) {
	var doc;
	if (data.type && collection._docTypePrototypes) doc = collection.newDocTypeObject(data.type); // use a type prototype if available
	if (!doc) doc = collection.newDocObject(); // if not - use the standard document prototype

	_.extend(doc, data);

	/*if (Meteor.isServer)*/ doc.__TRANSFORM__ = collection.name;

	return doc;
}

Collection_Prototype = {

	/** Collection **/

	name: '',
	collection: null,
	
	/** Get **/
	// Common get types

	getById: function (_id, projection) {
		if (_id instanceof Array) {
			return this.getByIds(_id, projection);
		} else {
			return this.collection.findOne(_id, projection);
		}
	},
	getById_cursor: function (_id, projection) {
		if (_id instanceof Array) {
			this.getByIds_cursor(_id, projection);
		} else {
			return this.collection.find(_id, projection);
		}
	},

	getByIds: function (_ids, projection) {
		return this.getByIds_cursor(_ids).fetch();
	},
	getByIds_cursor: function (_ids, projection) {
		return this.collection.find({
			_id: {$in: _ids}
		}, projection);
	},

	/** Extend Collection/Document Prototype **/

	/* Collection */

	extendCollection: function (object) {
		var proto = this.__proto__; // used by Opera
		if (!proto) proto = Object.getPrototypeOf(this); // new standard, most browsers support (IE9+)

		ExtendObject(proto, object);
	},

	/* Document */

	_docProtoype: null,

	getDocPrototype: function () {
		if (!this._docProtoype) {
			this._docProtoype = _.clone(Document_Prototype);
			this._docProtoype._collection = this; // add a reference to the collection itself as part of the prototype
												  // we add as part of the prototype to stop refernce to this being part of the copied in data => sent to client etc
		}

		return this._docProtoype;
	},
	newDocObject: function () {
		return Object.create(this.getDocPrototype());
	},
	extendDocument: function (object) {
		ExtendObject(this.getDocPrototype(), object);
	},

	/* Document Type */

	_docTypePrototypes: null, // we support the ability to use a different prototype depending on the 'type' of an instance (ie item types - question, answer, etc)
							  // problem with this, as usual, is one of load orders - we want to compile together so that the type extends the FINAL version of the document protoype
							  // => we do this final extension at startup [NOTE: even this might not be safe as we do some extending at startup time too but this needs to be eliminated anyway]

	getDocTypePrototype: function (_type) {
		this._docTypePrototypes = this._docTypePrototypes || {};

		if (!this._docTypePrototypes[_type]) {
			this._docTypePrototypes[_type] = {};
			
			Meteor.startup(function () {
				var proto = _.clone(this.getDocPrototype());
				this._docTypePrototypes[_type] = _.extend(proto, this._docTypePrototypes[_type]);
			}.bind(this));
		}

		return this._docTypePrototypes;
	},
	newDocTypeObject: function (_type) {
		var proto = this.getDocTypePrototype(_type);
		if (!proto) return null;

		return Object.create(proto);
	},
	extendDocumentType: function (_type, object) {
		ExtendObject(this.getDocTypePrototype(_type), object);
	},
}

Collection_Prototype.extendDocumentPrototype = Collection_Prototype.extendDocument; // backwards compat