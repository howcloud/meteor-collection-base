/** Publish Representation **/

/*

We do not provide a default implementation of this as it doesn't work
It doesn't work with fields which are more complex ie 'field.something'
Need to work out the consequences for representations for React components - should be an elegant enough way aroound this, though - just query all needed for the request and output 
=> do a look up to those on page load

Document_Prototype.getPublishRepresentation = function (_user_id) {
	var publish = {};

	var publishFields = this._collection.publishFields;	
	if (publishFields) {
		var self = this;

		_.each(publishFields, function (_publish, publishField) {
			if (_publish) publish[publishField] = self[publishField];
		});
	} else {
		_.each(this, function (value, variable) {
			publish[variable] = value;
		});
	}

	return publish;
}

*/

Document_Prototype.delete = function () {
	this._collection.collection.remove(this._id);
}

// set:
// performs a mongo set on this particular _id

Document_Prototype.set = function (set) {
	// Update mongodb

	this._collection.collection.update(this._id, {
		$set: set
	});

	// Update in place

	_.extend(this, set);
}

// update:
// performs a mongo style update on this particular _id

Document_Prototype.update = function (updates) {
	this._collection.collection.update(this._id, updates);
}

/**************************************************************************************************/
/** Subdocument Management **/

// setSubfields:
// performs sets on a subdocument

Document_Prototype.setSubfields = function (field, set) {
	var sets = {}
	this[field] = this[field] || {} // for in place updates

	_.each(set, function (value, variable) {
		
		// Create Query

		sets[field + '.' + variable] = value;

		// Update in place

		this[field][variable] = value;
		
	}.bind(this));

	this._collection.collection.update(this._id, {
		$set: sets
	});
}

/**************************************************************************************************/
/** Array of Subdocuments Management **/

Document_Prototype.subdocuments_array_add = function (field, subDocumentIdentifierKey, subDocumentIdentifierValue, data) {
	if (this.subdocuments_array_find(field, subDocumentIdentifierKey, subDocumentIdentifierValue)) return true; // already added

	// Construct Data

	var baseData = {} // the base data which we should insert as the subdocument is at a minimum the identifier key = value
	baseData[subDocumentIdentifierKey] = subDocumentIdentifierValue;

	// Update document

	var push = {}
	push[field] = _.extend(baseData, data || {});

	this._collection.collection.update(this._id, {
		$push: push
	});

	// In place update

	this[field] = this[field] || [];
	this[field].push(push[field]);

	// return

	return true;
}

Document_Prototype.subdocuments_array_update = function (field, subDocumentIdentifierKey, subDocumentIdentifierValue, data) {
	var currData = this.subdocuments_array_find(field, subDocumentIdentifierKey, subDocumentIdentifierValue, data);
	if (!currData) return false;

	// Update document

	var sets = {}
	_.each(data, function (value, variable) {
		sets[field+'.$.'+variable] = value;
	});

	var filter = {_id: this._id}
	filter[field+'.'+subDocumentIdentifierKey] = subDocumentIdentifierValue;

	this._collection.collection.update(filter, {
		$set: sets
	});

	// In place edit

	_.extend(currData, data);

	// return

	return true;
}

Document_Prototype.subdocuments_array_remove = function (field, subDocumentIdentifierKey, subDocumentIdentifierValue) {
	var currData = this.subdocuments_array_find(field, subDocumentIdentifierKey, subDocumentIdentifierValue);
	if (!currData) return true;

	// Update collection
	// see http://stackoverflow.com/questions/4588303/in-mongodb-how-do-you-remove-an-array-element-by-its-index/4588909#4588909 for why we do it like this [kinda crazy]
	// we could just pull from users where == currData (ie like we do for the in place edit) but there is an edge case where some other reuqest has updated this since we queried this document here

	var filter = {_id: this._id}
	filter[field+'.'+subDocumentIdentifierKey] = subDocumentIdentifierValue;

	var unset = {}
	unset[field+'.$'] = 1;

	var pull = {}
	pull[field] = null;

	this._collection.collection.update(filter, {
		$unset: unset
	});
	this._collection.collection.update(this._id, {
		$pull: pull
	});

	// In place edit

	this[field] = _.without(this[field], currData);

	// return

	return true;
}