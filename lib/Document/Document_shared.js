Document_Prototype = {

	/** Subfield Operations **/

	getSubfield: function (field, key, _default) {
		if (key) {
			return this[field] ? this[field][key] || _default : _default;
		} else {
			return this[field] || {};
		}
	},

	/** Array of Subdocuments Management **/

	subdocuments_array_find: function (field, subDocumentIdentifierKey, subDocumentIdentifierValue) {
		return _.find(this[field], function (subdocument) {
			return subdocument[subDocumentIdentifierKey] === subDocumentIdentifierValue ? true : false;
		});
	}

	// rest of these are defined only on the server side (removing, adding etc)

}