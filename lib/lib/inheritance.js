/*

This method is based off of the inheritance system by John Resig but is used directly on two objects which could be instantiated with Object.create
Extends base in place - ie pass it a clone if you want a fresh object instance

*/

var fnTest = /xyz/.test(function(){xyz;}) ? /\b_super\b/ : /.*/; // what is this for???

ExtendObject = function (base, extend) {

  var prototype = base;
  var _super = _.clone(base);
 
  // Copy the properties over onto the new prototype
  for (var name in extend) {
    // Check if we're overwriting an existing function
    prototype[name] = typeof extend[name] == "function" && typeof _super[name] == "function" ?
      (function(name, fn){
        return function() {
          // store the current value of super to restore it (ie in the case that there is some key set called _super already, which really shouldn't happen, hence needing to DELETE [see below])
          var tmp = this._super;
         
          // Add a new ._super() method that is the same method
          // but on the super-class
          this._super = _super[name];
         
          // The method only need to be bound temporarily, so we
          // remove it when we're done executing
          var ret = fn.apply(this, arguments);

          if (tmp === undefined) {
            // It's really important to DELETE if this was previously undefined (remember debugging this for 5 hours+ till like 3 am on 22/10/14???)
            // Point is - otherwise we end up with a mutated document with a new property that was not there before => _.isEqual will be false just in virtue of calling/trying to call super methods

            delete this._super;
          } else {
            this._super = tmp;
          }
         
          return ret;
        };
      })(name, extend[name]) :
      extend[name];
  }

  return prototype;

}